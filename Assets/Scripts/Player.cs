﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameObject _weapon;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FollowCrosshair();

        if(Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
    }


    //Function to make gun follow crosshair
    //this can be improved by including clamped movement left and right
    private void FollowCrosshair()
    {
        this.gameObject.transform.LookAt(Input.mousePosition);
    }

    private void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
        {
            Debug.LogError("Hit: " + hit.transform.gameObject);
        }
    }
}
